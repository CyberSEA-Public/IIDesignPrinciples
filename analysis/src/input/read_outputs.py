import concurrent.futures
import copy
from glob import glob

from networkx.utils.misc import to_tuple

from src.model.ImplicitInteraction import ImplicitInteraction
from src.utilities.utilities import read_file, find_substring

EXPLOITABILITY_OUTPUTS_CONTAIN_ATTACK = {'ManufacturingCell': True,
                                         'WastewaterSystem': True,
                                         'PayInfo': True,
                                         'PortTerminal': False,
                                         'ChemicalReactor': False}


def read_implicit_interaction_file(spec_folder):
    """
    Reads the contents of the C2KA-Tool output file into a string.

    This assumes that the C2KA-Tool output file is the only file in the output folder for a system

    :param spec_folder: the path to the folder containing all the system information
    :return: a string with all the contents of the C2KA-Tool output file
    """
    implicit_interaction_file = glob(spec_folder + '/Data/Outputs/*')[0]
    return read_file(implicit_interaction_file)


def get_implicit_interactions(spec_folder, threads=64):
    """
    Reads the C2KA-Tool output data into ImplicitInteraction objects

    :param spec_folder: the path to the folder containing all the system information
    (non-implicits, True by default)
    :param threads: the number of work units to break the specification parsing into
    :return: a list of all implicit interactions and their properties as outlined in the C2KA-Tool output
    """
    # get file content
    project_name = spec_folder.split('/')[2]
    implicit_interaction_file_content = read_implicit_interaction_file(
        spec_folder)
    lines = implicit_interaction_file_content.split('\n')

    # extract implicit interactions
    implicit_interaction_strings = []
    for i in range(len(lines)):
        if lines[i].endswith(': True') and '~>+' in lines[i]:
            j = i + 4
            while not lines[j].startswith('------------------------'):
                implicit_interaction_strings.append(lines[j])
                j += 1

    if threads == 1:
        return get_implicit_interactions_helper(implicit_interaction_strings,
                                                lines, project_name)
    else:
        # split lines into chunks
        line_chunks = [implicit_interaction_strings[i:i + len(implicit_interaction_strings) // threads]
                       for i in
                       range(0, len(implicit_interaction_strings), len(implicit_interaction_strings) // threads)]
        # get results from each chunk concurrently and combine
        results = []
        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = {executor.submit(get_implicit_interactions_helper, chunk, lines, project_name)
                       for chunk in line_chunks}
            for future in concurrent.futures.as_completed(futures):
                results.extend(future.result())
        return results


def get_implicit_interactions_helper(implicit_paths, lines, project_name):
    """
    A helper function for processing one work unit of the C2KA-Tool output file

    :param implicit_paths: the subset of implicit interactions to parse for this work unit
    :param lines: a reference to the entire C2KA-Tool output string, split into a list of lines
    :param project_name: the name of the system as given by the project folder name
    :return: a list of ImplicitInteraction objects for this work unit of the file to be parsed
    """
    implicit_interactions = []
    for i in range(len(implicit_paths)):
        # separate each string into the interaction and severity value
        severity_string, interaction_string = implicit_paths[i].lstrip().split('	\t')
        severity = float(severity_string[len('SEVERITY = '):])
        if severity > 0:  # this is an implicit interaction
            implicit_interaction = ImplicitInteraction()
            implicit_interaction.interaction = convert_interaction_text_to_tuple_list(interaction_string)
            implicit_interaction.severity = severity
            implicit_interaction.exploitability = get_exploitability(interaction_string, lines, project_name)

            implicit_interactions.append(implicit_interaction)
    return implicit_interactions


def get_exploitability(implicit_interaction, lines, project_name):
    """
    Finds the exploitability for a given implicit interaction within lines

    :param implicit_interaction: an implicit interaction string of the form:
        '<agent> -><type of interaction> <agent> -><type of interaction> <agent> ...'
    :param lines: a reference to the entire C2KA-Tool output string, split into a list of lines
    :param project_name: the name of the system as given by the project folder name
    :return: the exploitability of the given implicit_interaction
    """
    gap = 2 if project_name not in EXPLOITABILITY_OUTPUTS_CONTAIN_ATTACK.keys() or \
        EXPLOITABILITY_OUTPUTS_CONTAIN_ATTACK[project_name] else 1
    for i in range(len(lines)):
        if lines[i] == 'Implicit Interaction = 	' + implicit_interaction:
            return float(lines[i + gap].split('=')[1])


def get_intended_interactions(spec_folder):
    """
    Reads the intended interaction data from the data file. Each intended interaction is represented as a list of
    tuples, where each tuple is a direct interaction in the form:
        (<agent>, <type of interaction>, <agent>)

    :param spec_folder: the path to the folder containing all the system information
    :return: a list of tuples representing the intended interactions of the system
    """
    # get file content
    implicit_interaction_file_content = read_implicit_interaction_file(spec_folder)

    # find intended interactions in text
    implicit_interaction_header = '------------------------\nINTENDED INTERACTIONS   \n------------------------'
    intended_interactions = []
    for s in find_substring(implicit_interaction_file_content, implicit_interaction_header, '\n\n\n').split('\n'):
        if s:  # empty strings are falsey, so this skips empty strings
            # convert each string to tuple form
            intended_interactions.append(convert_interaction_text_to_tuple_list(s[1:]))

    # now we need to remove all the 'B' interactions with copies of the interaction with each 's' and 'E'
    i = 0
    while i < len(intended_interactions): # intended_interactions[i] is one intended interaction
        for j in range(len(intended_interactions[i])): # intended_interactions[i][j] is one direct interaction in # intended_interactions[i]
            if intended_interactions[i][j][1] == 'B':
                # change 'B' to 'S'
                intended_interactions[i][j] = (intended_interactions[i][j][0], 'S', intended_interactions[i][j][2])
                # create copy of intended_interactions[i] with an 'E' in intended_interactions[i][j][1]
                interaction_copy = copy.deepcopy(intended_interactions[i])
                interaction_copy[j] = (interaction_copy[j][0], 'E', interaction_copy[j][2])
                intended_interactions.append(interaction_copy)
        i += 1

    return intended_interactions


def convert_interaction_text_to_tuple_list(interaction_text):
    """
    Converts an interaction string into a list of tuples

    :param interaction_text: a string of the form:
        '<agent> -><type of interaction> <agent> -><type of interaction> <agent> ...'
    :return: a list of tuples of the form:
        (<agent>, <type of interaction>, <agent>)
    """
    interaction = []
    interaction_text = interaction_text.split()
    for i in range(len(interaction_text)):
        if interaction_text[i].startswith('->'):
            interaction.append((interaction_text[i - 1], interaction_text[i][-1], interaction_text[i + 1]))
    return interaction


def get_num_interactions(project_name):
    text = read_implicit_interaction_file(project_name).split()
    for i in range(len(text)):
        if text[i].startswith('----------') and text[i+1].startswith('TOTAL'):
            return to_tuple([int(x) for x in text[i+2].split('/')])
    
