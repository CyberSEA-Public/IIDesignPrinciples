import itertools, statistics

from networkx import pagerank, bridges
from networkx.algorithms.centrality import *
from networkx.algorithms.connectivity.cuts import *
from networkx.algorithms.cycles import simple_cycles
from networkx.algorithms.smallworld import omega

from src.model.CommunicationGraph import CommunicationGraph


def communication_graph_from_behaviors(state_transitions, concrete_behaviors, only_influencing_stimuli=True):
    """
    For a system of n agents, build an n by n adjacency matrix to represent a communication graph of the system

    :param state_transitions: a mapping of each agent to how their state changes from incoming stimuli
    :param concrete_behaviors: a mapping of each agent to the variables they define and reference in each state
    :return: an n by n matrix where each cell is all the ways the row agent can influence the column agent
    """
    # a 2D list where a cell specifies all the ways the row agent can influence the column agent
    num_agents = len(state_transitions)
    matrix = [[{'E': set(), 'S': set()} for column in range(num_agents)] for row in range(num_agents)]

    # get the list of agent names
    agents = list(state_transitions.keys())

    for agent in agents:
        # stimuli communication
        #
        # look at the stimuli that each agent sends and look for other agents that change state or
        # send their own stimuli in response to receiving the same stimuli
        for (next_stimuli, _) in state_transitions[agent].values():
            if next_stimuli != 'N':  # check all the non-neutral stimuli and compare to other agents
                for other_agent in list(set(agents) - set(agent)):
                    for (other_stimuli, other_state), (other_next_stimuli, other_next_state) in state_transitions[other_agent].items():
                        if only_influencing_stimuli:
                            if other_stimuli == next_stimuli and other_state != other_next_state:
                                # if state changes, then add to graph
                                matrix[agents.index(agent)][agents.index(other_agent)]['S'].add(next_stimuli)
                        else:
                            if other_stimuli == next_stimuli and (other_state != other_next_state or other_next_stimuli != 'N'):
                                # if state changes or sitmuli produces, then add to graph
                                matrix[agents.index(agent)][agents.index(other_agent)]['S'].add(next_stimuli)

                        

        # shared environment communication
        #
        # look at the variables references by each agent and look for an agents that define that variable
        for concrete_behavior in concrete_behaviors[agent].values():  # concrete_behaviors holds lists of variables defined and referenced in a state
            for r in concrete_behavior['ref']:
                for other_agent in list(set(agents) - set(agent)):
                    for other_concrete_behavior in concrete_behaviors[other_agent].values():
                        for d in other_concrete_behavior['def']:
                            if d == r:
                                matrix[agents.index(other_agent)][agents.index(agent)]['E'].add(r)

    # clear the diagonal
    for i in range(len(matrix)):
        matrix[i][i] = {'E': set(), 'S': set()}

    return CommunicationGraph(matrix, agents)
    

def fan_out(communication_graph):
    """
    calculate the number of ways each agent can influence other agents

    :param communication_graph: the communication graph of the system
    :return: A mapping of each agent name to the number of ways that agent can influence other agents
    """
    coupling_out = {}
    for i in range(communication_graph.num_rows()):
        coupling_out[communication_graph.labels[i]] = 0
        for j in range(communication_graph.num_columns()):
            coupling_out[communication_graph.labels[i]] += len(communication_graph.matrix[i][j]['S']) + \
                                                           len(communication_graph.matrix[i][j]['E'])
    return coupling_out


def fan_in(communication_graph):
    """
    calculate the number of ways each agent can be influenced by other agents

    :param communication_graph: the communication graph of the system
    :return: A mapping of each agent name to the number of ways that agent can be influenced by other agents
    """
    coupling_in = {}
    for i in range(communication_graph.num_rows()):
        coupling_in[communication_graph.labels[i]] = 0
        for j in range(communication_graph.num_columns()):
            coupling_in[communication_graph.labels[i]] += len(communication_graph.matrix[j][i]['S']) + \
                                                           len(communication_graph.matrix[j][i]['E'])
    return coupling_in


def mulitigraph_degree(communication_graph):
    """
    calculate the normalized degree centrality of a communication graph. This is the number of incoming and outgoing
    influences to each component

    :param communication_graph: the communication graph of the system
    :return: A mapping of each agent name to their normalized degree centrality
    """
    return degree_centrality(communication_graph.to_networkx_multigraph())


def degree(communication_graph):
    """
    calculate the normalized degree centrality of a communication graph. This is the number of incoming and outgoing
    influences to each component

    :param communication_graph: the communication graph of the system
    :return: A mapping of each agent name to their normalized degree centrality
    """
    return degree_centrality(communication_graph.to_networkx_graph())


def number_neighbors(communication_graph):
    """
    Calculate the number of neighbors that are either influenced by, or influence, each component

    :param communication_graph: the communication graph of the system
    :return: A mapping of each agent name to their number of neighbors
    """
    result = degree_centrality(communication_graph.to_networkx_graph().to_undirected())
    return result


def closeness(communication_graph):
    """
    Calculate the closeness (normalized harmonic centrality) of the given communication graph

    :param communication_graph: the communication graph of the system
    :return: A mapping of each agent name to their closeness centrality
    """
    networkx_graph = communication_graph.to_networkx_multigraph()
    result = harmonic_centrality(networkx_graph.reverse())
    return {k: v/(len(result)-1) for k, v in result.items()}


def betweenness(communication_graph):
    """
    Calculate the betweenness of the given communication graph

    :param communication_graph: the communication graph of the system
    :return: A mapping of each agent name to their betweenness centrality
    """
    networkx_graph = communication_graph.to_networkx_graph()
    result = betweenness_centrality(networkx_graph)
    return result


def page_rank(communication_graph):
    """
    Calculate the PageRank of the given communication graph

    :param communication_graph: the communication graph of the system
    :return: A mapping of each agent name to their pagerank
    """
    networkx_graph = communication_graph.to_networkx_multigraph()
    result = pagerank(networkx_graph)
    return result


def subgraph_coupling(communication_graph):
    """
    Calculate the number of edges in between edge subset of nodes in the given communication graph

    :param communication_graph: the communication graph of the system
    :return: A mapping of component subsets to number of edges between those components, normalized by subset size
    """
    networkx_graph = communication_graph.to_networkx_graph()
    subgraph_edges = {}
    for i in range(2, len(networkx_graph.nodes)):
        subgraph_edges.update({nodes: len(networkx_graph.subgraph(nodes).edges) / i
                               for nodes in itertools.combinations(networkx_graph.nodes, i)})
    return subgraph_edges


def eigenvector(communication_graph):
    return eigenvector_centrality(communication_graph.to_networkx_graph().to_undirected())


def find_bridges(communication_graph):
    return list(bridges(communication_graph.to_networkx_graph().to_undirected()))
    

def min_edge_cut(communication_graph):
    return minimum_edge_cut(communication_graph.to_networkx_graph().to_undirected())


def min_node_cut(communication_graph):
    return minimum_node_cut(communication_graph.to_networkx_graph().to_undirected())


def cycles(communication_graph):
    return list(simple_cycles(communication_graph.to_networkx_graph()))


def small_worldedness(communication_graph):
    return omega(communication_graph.to_networkx_graph().to_undirected())
    
def centralization(communication_graph):
    n = communication_graph.num_rows()
    degrees = {k:v*(n-1) for (k,v) in degree(communication_graph).items()}
    max_value = max(degrees.values())
    
    # sum of degree differences for the given graph
    numerator = sum([max_value-v for v in degrees.values()])
    # sum of degree differences for a star with each bidirectional communication to each branch
    denominator = (n - 1) * (2*(n-1) - 2)
    #normalize degree differences by theoretical maximum for graph of given size
    return numerator / denominator
    

def degree_variance(communication_graph):
    return statistics.variance([v*(communication_graph.num_rows()-1) for v in mulitigraph_degree(communication_graph).values()])


def amount_broadcasting(communication_graph, as_percent=False):
    """
    Determine the percentage of communication via stimuli that are broadcast
    """
    num_stim = 0
    num_broadcast_stim = 0
    for i in range(len(communication_graph.matrix)):
        stim_sent = {}
        for j in range(len(communication_graph.matrix[i])):
            for s in communication_graph.matrix[i][j]['S']:
                stim_sent[s] = stim_sent.get(s, 0) + 1
        num_broadcast_stim += len([v for v in stim_sent.values() if v > 1])
        num_stim += len(stim_sent)
    return num_broadcast_stim / num_stim if as_percent else num_broadcast_stim
    
def average_parallelism(intended_interaction_tree, intended_interaction_paths):
    # throw out agent names and type of edges
    edges = [[edge[0][0], edge[1][0]] for edge in intended_interaction_tree]

    nodes = set([item for sublist in edges for item in sublist])

    longest_path_length = max([len(x) for x in intended_interaction_paths]) +1
    number_nodes_in_tree = len(nodes)
    
    return number_nodes_in_tree / longest_path_length



