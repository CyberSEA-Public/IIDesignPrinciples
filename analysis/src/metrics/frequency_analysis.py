def generate_histogram(items):
    """
    Count the occurrences of each item in the passed collection and return a histogram as a dictionary

    :param items: the collection to generate a histogram for
    :return: a dictionary mapping each item in the input collection to number of occurrences in the collection
    """
    histogram = {}
    for item in items:
        histogram[item] = histogram.get(item, 0) + 1
    return histogram


def ratio(hist1, hist2):
    """
    Calculate a ratio between the frequencies of items in two histograms

    The ratio is hist1/hist2
    0 means the item is only in hist2
    None means the item is only in hist1

    :param hist1: the first histogram
    :param hist2: the second histogram
    :return: a dictionary mapping each item of the input histograms to a ratio of occurrences
    """
    ratios = {}
    for key in list(set(hist1.keys()) | set(hist2.keys())):
        if key not in hist2.keys():
            ratios[key] = 'NaN'
        elif key not in hist1.keys():
            ratios[key] = 0
        else:
            ratios[key] = hist1[key] / hist2[key]
    return ratios


def generate_weighted_component_histogram(implicit_interactions):
    """
    Weigh the occurrences of each component in the passed implicit interactions and return a histogram as a dictionary

    :param implicit_interactions: the implicit interactions to generate a histogram for
    :return: a dictionary mapping each component in the implicit interactions to a weighted occurrence
    """
    components_weighted = {}
    for i in implicit_interactions:
        # add left side of each direct interaction in implicit interaction
        for direct in i.interaction:
            components_weighted[direct[0]] = components_weighted.get(direct[0], 0) + i.exploitability
        # add the right side of the last direct interaction
        last_component = i.interaction[-1][-1]
        components_weighted[last_component] = components_weighted.get(last_component, 0) + i.exploitability
    return components_weighted


def generate_weighted_direct_interaction_histogram(implicit_interactions):
    """
    Weigh the occurrences of each direct interaction in the passed implicit interactions and return a
    histogram as a dictionary

    :param implicit_interactions: the implicit interactions to generate a histogram for
    :return: a dictionary mapping each direct interaction in the implicit interactions to a weighted occurrence
    """
    direct_interactions_weighted = {}
    for i in implicit_interactions:
        for direct in i.interaction:
            direct_interactions_weighted[direct] = direct_interactions_weighted.get(direct, 0) + i.exploitability
    return direct_interactions_weighted
