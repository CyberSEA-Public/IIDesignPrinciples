from scipy.stats import pearsonr, spearmanr


def correlate(x, y, correlation_type='pearson'):
    """
    return the correlation between two set of data

    :param x: the first data set
    :param y: the second data set
    :param correlation_type: the type of correlation to calculate (pearson/spearman, default: pearson)
    :return: the correlation value
    """
    if correlation_type == 'spearman':
        return spearmanr(x, y)
    elif correlation_type == 'pearson':
        return pearsonr(x, y)
