import pandas as pd


def print_implicit_interactions(implicit_interactions, newline=True, give_id=True):
  for i in range(len(implicit_interactions)):
    s = ('$x_{'+str(i)+'}$ & $' if give_id else '$') 
    p = implicit_interactions[i]
    for di in p:
      s += '\\Agent{'+di[0]+'}'
      s += '\\to'+di[1]
    s += '\\Agent{'+p[-1][-1]+'}$' + ('\\\\' if newline else '')
    print(s)


def print_interaction_contribution_ratio_table(implicit_frequency, intended_frequency, contribution_ratio):
  for key, value in sorted(contribution_ratio.items(), key=lambda x: (x[1] == 'NaN', x[1]), reverse=True):
    print('$\\Agent{',key[0],'}\\to',key[1],'\\Agent{',key[-1],'}$ & ',str(implicit_frequency.get(key,0)),' & ',str(intended_frequency.get(key,0)),' & ',str(value),'\\\\',sep='')
    

def print_dataframe_latex(x, title_wrapper=lambda x: ('\\ref{'+str(x)+'}')):
  print(x.to_latex(col_space=4))

