import sys

import pandas as pd

from src.utilities.plotting import plot_histogram, plot_scatter


def print_full_dataframe(df, title='', output_stream=sys.stdout):
    """
    print a dataframe in full without columns or rows being cutoff

    :param df: the dataframe to print
    :param title: a title to display above the dataframe
    :param output_stream: the output stream to write the dataframe to
    :return: None
    """
    with pd.option_context('display.max_rows', None, 'display.max_columns', None, 'display.width', None):
        print(title, ':', sep='', file=output_stream)
        print(df, file=output_stream)


def plotting(implicit_interactions, communication_graph, write_to_file, include_exploitability, include_severity):
    """
    create and display a series of plots of the results

    :param implicit_interactions: the set of implicit interactions of a system
    :param communication_graph: the communication graph of the system
    :param write_to_file: if true, save plots to files, otherwise display in window
    :param include_exploitability: if true, we have exploitability data so plots related to this can be displayed
    :param include_severity: if true, we have severity data so plots related to this can be displayed
    :return: None
    """
    # plot histogram of implicit interaction length
    plot_histogram([len(x.interaction) for x in implicit_interactions],
                   title='Implicit Interaction Length', x_axis='Interaction Length', y_axis='Frequency',
                   save=write_to_file)

    if include_exploitability:
        # plot exploitability histogram
        plot_histogram([x.exploitability for x in implicit_interactions],
                       title='Exploitability Frequencies', x_axis='Exploitability', y_axis='Frequency',
                       x_lim=(-0.1, 1.1),
                       save=write_to_file)
        # plot exploitability vs interaction length
        plot_scatter([x.exploitability for x in implicit_interactions],
                     [len(y.interaction) for y in implicit_interactions],
                     x_axis='exploitability', y_axis='interaction length',
                     title='Implicit Interaction Length vs Exploitability',
                     show_frequency=True, x_lim=(-0.1, 1.1), save=write_to_file)

    if include_severity:
        # plot severity histogram
        plot_histogram([x.severity for x in implicit_interactions],
                       title='Severity Frequencies', x_axis='Severity', y_axis='Frequency', x_lim=(-0.1, 1.1),
                       save=write_to_file)

    if include_exploitability and include_severity:
        # plot exploitability vs severity
        plot_scatter([x.exploitability for x in implicit_interactions],
                     [y.severity for y in implicit_interactions],
                     x_axis='exploitability', y_axis='severity',
                     title='Implicit Interaction Severity vs Exploitability',
                     show_frequency=True, x_lim=(-0.1, 1.1), y_lim=(-0.1, 1.1), save=write_to_file)

    # draw communication graph
    communication_graph.draw_graph(save=write_to_file)


def write_to_spreadsheet(titled_dataframes, spreadsheet_name):
    with pd.ExcelWriter(spreadsheet_name + '.xlsx') as writer:
        for sheet, df in titled_dataframes.items():
            df.to_excel(writer, sheet_name=sheet, engine='xlsxwriter')
