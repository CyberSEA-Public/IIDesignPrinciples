import matplotlib.pyplot as plt

from src.metrics.correlate import correlate
from src.metrics.frequency_analysis import generate_histogram


def plot_scatter(x, y, title='', x_axis='', y_axis='', x_lim=None, y_lim=None, correlation_type=None,
                 show_frequency=False, save=False, display=True):
    """
    Plot a scatter plot of x and y data

    :param x: the independent variable to plot
    :param y: the dependent variable to plot
    :param title: an optional title for the plot
    :param x_axis: an optional x axis label for the plot
    :param y_axis: an optional y axis label for the plot
    :param show_frequency: a boolean value denoting whether or not points should be colored to show frequency
    :param y_lim: an optional tuple consisting of the upper and lower bounds of the y-axis
    :param x_lim: an optional tuple consisting of the upper and lower bounds of the x-axis
    :param save: an optional boolean that, if true, saves the figure to a file with the plot title
    :param correlation_type: an optional label for spearman or pearson correlation of the two variables
    :return: None
    """
    plt.figure()
    if show_frequency:
        data = generate_histogram([(a, b) for a, b in zip(x, y)])
        fig, ax = plt.subplots()
        plot = ax.scatter([a for a, _ in data.keys()], [b for _, b in data.keys()],
                          c=list(data.values()), cmap='Reds', edgecolors='black', linewidths=0.7)
        cb = fig.colorbar(plot, ax=ax)
        cb.set_label('Frequency')
    else:
        plt.scatter(x, y)
    if correlation_type is not None:
        plt.text(0, 0, correlation_type + ': ' + str(correlate(x, y, correlation_type=correlation_type)))
    format_plot(title=title, x_axis=x_axis, y_axis=y_axis, x_lim=x_lim, y_lim=y_lim, save=save, display=display)


def plot_histogram(data, title='', x_axis='', y_axis='', x_lim=None, y_lim=None, save=False, display=True, fig=None):
    """
    Plot a histogram of a single dimension collection

    :param data: the data series to plot
    :param title: an optional title for the plot
    :param x_axis: an optional x axis label for the plot
    :param y_axis: an optional y axis label for the plot
    :param y_lim: an optional tuple consisting of the upper and lower bounds of the y-axis
    :param x_lim: an optional tuple consisting of the upper and lower bounds of the x-axis
    :param save: an optional boolean that, if true, saves the figure to a file with the plot title
    :return: None
    """
    if fig is None:
        plt.figure()
        plt.hist(data)
    else:
        fig.hist(data)
    format_plot(title=title, x_axis=x_axis, y_axis=y_axis, x_lim=x_lim, y_lim=y_lim, save=save, display=display, fig=fig)


def format_plot(title='', x_axis='', y_axis='', x_lim=None, y_lim=None, save=False, display=True, fig=None):
    """
    Format plot according to following parameters

    :param title: an optional title for the plot
    :param x_axis: an optional x axis label for the plot
    :param y_axis: an optional y axis label for the plot
    :param y_lim: an optional tuple consisting of the upper and lower bounds of the y-axis
    :param x_lim: an optional tuple consisting of the upper and lower bounds of the x-axis
    :param save: an optional boolean that, if true, saves the figure to a file with the plot title
    :return: None
    """
    if fig is None:
        plt.title(title)
        plt.xlabel(x_axis)
        plt.ylabel(y_axis)
        if x_lim:
            plt.xlim(x_lim[0], x_lim[1])
        if y_lim:
            plt.ylim(y_lim[0], y_lim[1])
    else:
        fig.set_title(title)
        fig.set_xlabel(x_axis)
        fig.set_ylabel(y_axis)
        if x_lim:
            fig.set_xlim(x_lim[0], x_lim[1])
        if y_lim:
            fig.set_ylim(y_lim[0], y_lim[1])
    if save:
        plt.savefig(title.replace(' ', '_') + '.png')
    if display and not save:
        plt.show()
       
    
