import itertools, json
from networkx.algorithms.centrality.degree_alg import out_degree_centrality
from networkx.algorithms.efficiency_measures import global_efficiency
from networkx.algorithms.shortest_paths.generic import average_shortest_path_length
import pandas as pd
import numpy as np

from statistics import mean, variance, median
from scipy.stats import skew
from src.input.read_outputs import get_implicit_interactions, get_intended_interactions, get_num_interactions
from src.input.read_specs import read_specs, get_intended_interaction_tree
from src.metrics.frequency_analysis import generate_histogram, ratio
from src.metrics.graphical_analysis import centralization, communication_graph_from_behaviors, cycles, degree_variance, find_bridges, min_edge_cut, min_node_cut, amount_broadcasting, small_worldedness, mulitigraph_degree, average_parallelism
from src.output.output import print_full_dataframe, write_to_spreadsheet
from src.utilities.utilities import extract_components, combine_into_dataframe, read_file


SPECIFICATION_FOLDER = 'resources/data'
     
SYSTEM_TO_USE = ['MCCS1','MCCS2','MCCS3','MCCS4','MCCS5','MCCS6','MCCS7','MCCS8','MCCS9','MCCS10']
	
def correlate_two_measures(m1, m2):
    metrics = {'m1': m1, 'm2': m2}
    _, correlations = combine_into_dataframe(metrics, list(set.union(set(m1.keys()), set(m2.keys()))))
    return correlations['m1']['m2']

def analyze_system(system_name):
    print(system_name)
    project_name = SPECIFICATION_FOLDER + '/' + system_name
    
    configuration = json.loads(read_file(project_name + "/Data/Specs/" + project_name.split('/')[-1] + ".json"))

    state_transitions, concrete_behaviors, _ = read_specs(project_name)
    communication_graph = communication_graph_from_behaviors(state_transitions, concrete_behaviors, only_influencing_stimuli=False)
    intended_interactions = get_intended_interactions(project_name)
    intended_interaction_tree = get_intended_interaction_tree(project_name)
    implicit_interactions = get_implicit_interactions(project_name, threads=1)
    implicit_paths = [i.interaction for i in implicit_interactions]

    _, nun_total_interactions = get_num_interactions(project_name)
    num_behaviors = sum(len(x.items()) for x in concrete_behaviors.values())
    num_stimuli = len(set([x for x,_ in list(state_transitions.values())[0].keys()]))
    
    component_implicit_frequency = generate_histogram(extract_components(implicit_paths))
    component_intended_frequency = generate_histogram(extract_components(intended_interactions))
    component_contribution_ratio = ratio(component_implicit_frequency, component_intended_frequency)
    
    DI_implicit_frequency = generate_histogram(list([o for o in implicit_paths for o in o]))
    DI_intended_frequency = generate_histogram(list([o for o in intended_interactions for o in o]))
    DI_contribution_ratio = ratio(DI_implicit_frequency, DI_intended_frequency)

    external_stimuli = []#configuration['external_stim']

    #print(correlate_two_measures(mulitigraph_degree(communication_graph), component_implicit_frequency))
    #communication_graph.draw_graph()
    return {
        'Number of intended interactions:' : len(intended_interactions),
        'Number of Implicit Interactions'  : len(implicit_interactions),
        'Total Number of Interactions': nun_total_interactions,
        'Percent of interactions that are implicit': len(implicit_interactions)/nun_total_interactions,
        'Number of Agents': len(component_contribution_ratio),
        'Number of Direct Interactions': len(DI_contribution_ratio),
        'Number of def/ref pairs': communication_graph.num_E(),
        'Cyclomatic Complexity': len(DI_contribution_ratio) - len(component_contribution_ratio) + 2,
        'Average coupling': mean(mulitigraph_degree(communication_graph).values()),
        'Number of Stimuli': num_stimuli,
        'Number of Behaviors': num_behaviors,
        "Number of 'boxes' in C2KA spec": num_behaviors * num_stimuli,
        'Variance in component contributon ratios' : variance(component_contribution_ratio.values()) if not any([str(c) == 'NaN' for c in component_contribution_ratio.values()]) else np.nan,
        'Variance in direct interaction contributon ratios' : variance(DI_contribution_ratio.values()) if not any([str(c) == 'NaN' for c in DI_contribution_ratio.values()]) else np.nan,
        'Minimum edge cut size (undirected graph):': len(min_edge_cut(communication_graph)),
        'Minimum node cut size (undirected graph):': len(min_node_cut(communication_graph)),
        'Number bridges (undirected graph):': len(find_bridges(communication_graph)),
        'Number cycles (directed graph):': len(cycles(communication_graph)),
        'Largest cycle': len(sorted(cycles(communication_graph), key=lambda x: len(x))[-1]) if len(cycles(communication_graph)) > 0 else 0,
        'Average cycle size': mean([len(x) for x in cycles(communication_graph)]) if len(cycles(communication_graph)) > 0 else 0,
        'Median cycle size': median([len(x) for x in cycles(communication_graph)]) if len(cycles(communication_graph)) > 0 else 0,
        'Global efficiency (undirected)': global_efficiency(communication_graph.to_networkx_graph().to_undirected()),
        'Average shortest path length': average_shortest_path_length(communication_graph.to_networkx_multigraph()),
        #'Omega (undirected graph):': small_worldedness(communication_graph),
        'Centralization (directed graph):': centralization(communication_graph),
        'Degree variance (directed multigraph):': degree_variance(communication_graph),
        'Average number of infulencable agents': mean(list(out_degree_centrality(communication_graph.to_networkx_graph()).values())),
        'Average number of ways to spread influence': mean(list(out_degree_centrality(communication_graph.to_networkx_multigraph()).values())),
        'Percentage of stimuli that are broadcast': amount_broadcasting(communication_graph, as_percent=True),
        'Number broadcasted stimuli': amount_broadcasting(communication_graph),
        'Skewedness of agent contribution ratios': skew(list(component_contribution_ratio.values())) if not any([str(c) == 'NaN' for c in component_contribution_ratio.values()]) else np.nan,
        'Skewedness of direct interaction contribution ratios': skew(list(DI_contribution_ratio.values())) if not any([str(c) == 'NaN' for c in DI_contribution_ratio.values()]) else np.nan,
        'Number Intended Interactions with only Stimuli:': len([x for x in intended_interactions if not any([y[1]=='E' for y in x])]),
        'Average Parallelism': average_parallelism(intended_interaction_tree, intended_interactions),
        'Number Oversights': len([x for x in DI_contribution_ratio.values() if str(x) == 'NaN']),
        'Number External Stimuli': len(external_stimuli)
        }


def build_dataset(systems, save_as_spreadsheet=False, print_results=False, output_spreadsheet_name='results'):
	x = pd.DataFrame.from_dict({s: analyze_system(s) for s in systems}, orient='index')
	x.fillna(x.mean(), inplace=True)

	c = x.corr(method='spearman')
	if save_as_spreadsheet:
		write_to_spreadsheet({'results':x, 'correlations':c}, output_spreadsheet_name)
	c = c.stack().reset_index()

	c.columns = ['variable1', 'variable2', 'correlation']
	mask_dups = (c[['variable1', 'variable2']].apply(frozenset, axis=1).duplicated()) | (c['variable1']==c['variable2']) 
	c = c[~mask_dups]
	c = c.sort_values('correlation', ascending=False)
	if print_results:
		print_full_dataframe(c[c['variable1'].isin(['Number of Implicit Interactions', 'Percent of interactions that are implicit']) |
	    	c['variable2'].isin(['Number of Implicit Interactions', 'Percent of interactions that are implicit'])])
	return x


if __name__ == '__main__':
	build_dataset(SYSTEM_TO_USE, save_as_spreadsheet=True, print_results=True)
