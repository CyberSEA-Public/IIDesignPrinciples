import pandas as pd
import numpy as np
import statsmodels.api as sm

from sklearn import datasets, linear_model
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score
from scipy import stats
from itertools import chain, combinations

from compute_relationships import build_dataset

MCCS_VARIATIONS = ['MCCS1','MCCS2','MCCS3','MCCS4','MCCS5','MCCS6','MCCS7','MCCS8','MCCS9','MCCS10']
MCCS_INPUT_VARIABLES = ['Number of intended interactions:', 'Cyclomatic Complexity', 'Number cycles (directed graph):', 'Largest cycle']

def powerset(iterable):
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(1, len(s)+1))
    
    
def do_regression(dataset, input_variables, output_variable):

    X = dataset[input_variables]
    y = dataset[output_variable]

    regr = linear_model.LinearRegression()
    regr.fit(X, y)
    return regr
    

def get_coefficients(dataset, input_variables, regression_model):
    return pd.concat([pd.DataFrame(dataset[input_variables].columns), pd.DataFrame(np.transpose(regression_model.coef_))], axis = 1) 
    

def get_r_squared(dataset, input_variables, output_variable, regression_model):
    X = dataset[input_variables]
    y = dataset[output_variable]

    yhat = regression_model.predict(X)
    SS_Residual = sum((y-yhat)**2)       
    SS_Total = sum((y-np.mean(y))**2)     
    r_squared = 1 - (float(SS_Residual))/SS_Total
    return r_squared


def get_regression_summary(dataset, input_variables, output_variable, regression_model):
    X = dataset[input_variables]
    y = dataset[output_variable]

    X2 = sm.add_constant(X)
    est = sm.OLS(y, X2)
    est2 = est.fit()
    return est2.summary()


def regression_for_combination_of_variables(dataset, input_variables, output_variable):
    r_squared = {}
    for v in powerset(input_variables):
        model = do_regression(dataset, list(v), output_variable)
        r_squared[tuple(v)] = get_r_squared(dataset, list(v), output_variable, model)
    return r_squared


df = build_dataset(MCCS_VARIATIONS)
r_squared = regression_for_combination_of_variables(df, MCCS_INPUT_VARIABLES, 'Percent of interactions that are implicit')
print()
print('MCCS prevalecne of implicit interactions')
for k in r_squared.keys():
    print('\t', k, ':', round(r_squared[k], 4))
    
df = build_dataset(MCCS_VARIATIONS)
r_squared = regression_for_combination_of_variables(df, MCCS_INPUT_VARIABLES, 'Number of Implicit Interactions')
print()
print('MCCS number of implicit interactions')
for k in r_squared.keys():
    print('\t', k, ':', round(r_squared[k], 4))
    
    