import itertools, json, datetime 
from networkx.algorithms.centrality.degree_alg import out_degree_centrality
from networkx.algorithms.efficiency_measures import global_efficiency
from networkx.algorithms.shortest_paths.generic import average_shortest_path_length
import pandas as pd
import numpy as np

from statistics import mean, variance, median
from scipy.stats import skew
from src.input.read_outputs import get_implicit_interactions, get_intended_interactions, get_num_interactions
from src.input.read_specs import read_specs, get_intended_interaction_tree
from src.metrics.frequency_analysis import generate_histogram, ratio
from src.metrics.graphical_analysis import centralization, communication_graph_from_behaviors, cycles, degree_variance, find_bridges, min_edge_cut, min_node_cut, amount_broadcasting, small_worldedness, mulitigraph_degree, average_parallelism, fan_in, fan_out, eigenvector, page_rank
from src.output.output import print_full_dataframe, write_to_spreadsheet
from src.utilities.utilities import extract_components, combine_into_dataframe, read_file


SPECIFICATION_FOLDER = 'resources/data'

SYSTEM_TO_USE = ['MCCS1','MCCS2','MCCS3','MCCS4','MCCS5','MCCS6','MCCS7','MCCS8','MCCS9','MCCS10']

def analyze_system(system_name):
    print(system_name)
    project_name = SPECIFICATION_FOLDER + '/' + system_name
    
    configuration = json.loads(read_file(project_name + "/Data/Specs/" + project_name.split('/')[-1] + ".json"))

    state_transitions, concrete_behaviors, _ = read_specs(project_name)
    communication_graph = communication_graph_from_behaviors(state_transitions, concrete_behaviors, only_influencing_stimuli=False)
    intended_interactions = get_intended_interactions(project_name)

    degree_centrality = mulitigraph_degree(communication_graph)
    degree_centrality = {k: degree_centrality[k]*(len(degree_centrality.keys())-1) for k in degree_centrality.keys()}
    degree_in = fan_in(communication_graph)
    degree_out = fan_out(communication_graph)
    eigen = eigenvector(communication_graph)
    pagerank = page_rank(communication_graph)

    number_intended = len(intended_interactions)
    number_agents = len(degree_centrality)
    number_DI = communication_graph.num_DI()

    cyclomatic_complexity = number_DI - number_agents + 2
    cycle_list = cycles(communication_graph)
    number_cycles = len(cycle_list)
    largest_cycle_size = len(sorted(cycle_list, key=lambda x: len(x))[-1]) if len(cycle_list) > 0 else 0
    median_cycle_size = median([len(x) for x in cycle_list]) if len(cycle_list) > 0 else 0
    degree_varaince = degree_variance(communication_graph)
    
    print(degree_centrality)
    print(degree_in)
    print(degree_out)
    print(eigen)
    print(pagerank)
    
    print(number_intended)
    print(number_agents)
    print(number_DI)
    print(cyclomatic_complexity)
    print(number_cycles)
    print(largest_cycle_size)
    print(median_cycle_size)
    print(degree_varaince)


def build_dataset(systems):
	times = {}

	global_start = datetime.datetime.now()
	for s in systems:
	    local_start = datetime.datetime.now() 
	    analyze_system(s)
	    local_end = datetime.datetime.now()
	    
	    local_time = local_end - local_start
	    times[s] = local_time
	    print('\ttime to run: ' + str(local_time))
	global_end = datetime.datetime.now()
	
	total_time = global_end - global_start
	print()
	print()
	print('total time to run: ' + str(total_time))
	print('average time to run: ' + str(total_time / len(systems)))
	with open('graph_runtimes.txt', 'w', encoding='utf-8') as file:
            file.write('\n'.join([k+':'+str(v) for k,v in times.items()]))
	with open('graph_runtimes.txt', 'a', encoding='utf-8') as file:
            file.write('\ntotal time to run:' + str(total_time))
            file.write('\naverage time to run:' + str(total_time / len(systems)))
	
	
	
if __name__ == '__main__':
	build_dataset(SYSTEM_TO_USE)
