import itertools, json
from networkx.algorithms.centrality.degree_alg import out_degree_centrality
from networkx.algorithms.efficiency_measures import global_efficiency
from networkx.algorithms.shortest_paths.generic import average_shortest_path_length
from sklearn.cluster import KMeans
import pandas as pd
import numpy as np

from statistics import mean, variance, median
from scipy.stats import skew
from src.input.read_outputs import get_implicit_interactions, get_intended_interactions, get_num_interactions
from src.input.read_specs import read_specs, get_intended_interaction_tree
from src.metrics.frequency_analysis import generate_histogram, ratio
from src.metrics.graphical_analysis import centralization, communication_graph_from_behaviors, cycles, degree_variance, find_bridges, min_edge_cut, min_node_cut, amount_broadcasting, small_worldedness, mulitigraph_degree, average_parallelism
from src.output.output import print_full_dataframe, write_to_spreadsheet
from src.utilities.utilities import extract_components, combine_into_dataframe, read_file


SPECIFICATION_FOLDER = 'resources/data'

SYSTEM_TO_USE = ['MCCS1','MCCS2','MCCS3','MCCS4','MCCS5','MCCS6','MCCS7','MCCS8','MCCS9','MCCS10']
	

def analyze_system(system_name):
    print(system_name)
    project_name = SPECIFICATION_FOLDER + '/' + system_name
    
    configuration = json.loads(read_file(project_name + "/Data/Specs/" + project_name.split('/')[-1] + ".json"))

    state_transitions, concrete_behaviors, _ = read_specs(project_name)
    communication_graph = communication_graph_from_behaviors(state_transitions, concrete_behaviors, only_influencing_stimuli=False)
    intended_interactions = get_intended_interactions(project_name)
    intended_interaction_tree = get_intended_interaction_tree(project_name)
    implicit_interactions = get_implicit_interactions(project_name, threads=1)
    implicit_paths = [i.interaction for i in implicit_interactions]

    _, nun_total_interactions = get_num_interactions(project_name)
    num_behaviors = sum(len(x.items()) for x in concrete_behaviors.values())
    num_stimuli = len(set([x for x,_ in list(state_transitions.values())[0].keys()]))
    
    component_implicit_frequency = generate_histogram(extract_components(implicit_paths))
    component_intended_frequency = generate_histogram(extract_components(intended_interactions))
    component_contribution_ratio = ratio(component_implicit_frequency, component_intended_frequency)
    
    DI_implicit_frequency = generate_histogram(list([o for o in implicit_paths for o in o]))
    DI_intended_frequency = generate_histogram(list([o for o in intended_interactions for o in o]))
    DI_contribution_ratio = ratio(DI_implicit_frequency, DI_intended_frequency)

    external_stimuli = []#configuration['external_stim']

    return {
        'Number of intended interactions:' : len(intended_interactions),
        'Number of Agents': len(component_contribution_ratio),
        'Number of Direct Interactions': len(DI_contribution_ratio),
        'Cyclomatic Complexity': len(DI_contribution_ratio) - len(component_contribution_ratio) + 2,
        'Number of Stimuli': num_stimuli,
        'Number of Behaviors': num_behaviors,
        "Number of 'boxes' in C2KA spec": num_behaviors * num_stimuli,
        'Number cycles (directed graph):': len(cycles(communication_graph)),
        'Largest cycle': len(sorted(cycles(communication_graph), key=lambda x: len(x))[-1]) if len(cycles(communication_graph)) > 0 else 0,
        'Average cycle size': mean([len(x) for x in cycles(communication_graph)]) if len(cycles(communication_graph)) > 0 else 0,
        'Degree variance (directed multigraph):': degree_variance(communication_graph),
        }
        

def build_dataset(systems):
	x = pd.DataFrame.from_dict({s: analyze_system(s) for s in systems}, orient='index')
	x.fillna(x.mean(), inplace=True)
	
	model = KMeans(n_clusters=4)
	model.fit(x)
	all_predictions = model.predict(x)
	
	clusters = {system_design:model.predict([x.loc[system_design, : ]]) for system_design in x.index.values}
	for k,v in clusters.items():
	    print(k, v, sep=': ')

	



	

if __name__ == '__main__':
	build_dataset(SYSTEM_TO_USE)
