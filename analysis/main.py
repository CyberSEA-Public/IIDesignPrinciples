import sys,json

import pandas as pd

from src.input.read_outputs import get_intended_interactions, get_implicit_interactions
from src.input.read_specs import read_specs, get_intended_interaction_tree
from src.metrics.frequency_analysis import generate_histogram, ratio
from src.metrics.graphical_analysis import communication_graph_from_behaviors, degree, eigenvector, page_rank, min_edge_cut, min_node_cut, find_bridges, cycles, mulitigraph_degree, small_worldedness, centralization, degree_variance, average_parallelism
from src.utilities.utilities import extract_components, combine_into_dataframe, read_file
from src.utilities.plotting import plot_histogram
from src.output.output import print_full_dataframe
from src.output.latex_formatting import print_implicit_interactions, print_interaction_contribution_ratio_table


SPECIFICATION_FOLDER = 'resources/data'


def get_first_occurence_differences(edges):
    first_occurence = {}
    direct_interactions = set()
    
    for edge in edges:
        if edge[2] == 'B':
            direct_interactions.add((edge[0][1], 'S', edge[1][1]))
            direct_interactions.add((edge[0][1], 'E', edge[1][1]))
        else:
    	    direct_interactions.add((edge[0][1], edge[2], edge[1][1]))
 
        if edge[0][1] not in first_occurence.keys():
    	    first_occurence[edge[0][1]] = edge[0][0]
        if edge[1][1] not in first_occurence.keys():
    	    first_occurence[edge[1][1]] = edge[1][0]

    return {DI: abs(first_occurence[DI[0]]-first_occurence[DI[-1]]) for DI in direct_interactions}


def correlate_two_measures(m1, m2):
    metrics = {'m1': m1, 'm2': m2}
    _, correlations = combine_into_dataframe(metrics, list(set.union(set(m1.keys()), set(m2.keys()))))
    return correlations['m1']['m2']


if __name__ == '__main__':
    project_name = SPECIFICATION_FOLDER

    # read the agent specification files for the project into a dictionary
    #
    # each item maps an agent name to a dictionary that describes the agent's specifications
    #
    # each item in the specification maps a pair of incoming stimulus and start state to a
    # pair of next stimulus and next state
    state_transitions, concrete_behaviors, stimuli_from_concrete_behavior = read_specs(project_name)

    # build a communication graph of the system using the agents specifications
    #
    # This will show all the ways each agent can influence, and be influenced by, other agents in the system
    communication_graph = communication_graph_from_behaviors(state_transitions, concrete_behaviors, only_influencing_stimuli=False)

    # parse the intended interactions listed in the C2KA-Tool output file for a system into a list
    #
    # each element in the list represents an intended interaction, and is a list of tuples
    #
    # each tuple in an intended interaction is one direct interaction of the form:
    #       (<source>, <interaction type>, <sink>)
    intended_interactions = get_intended_interactions(project_name)

    # results
    print('System:', project_name)
    print()
    print('Communication Graph:')
    print(communication_graph.label_edges_by_interaction_type().as_adjacency_matrix_string())
    print(communication_graph.as_adjacency_matrix_string())
    print()
    print('Number intended interactions:', len(intended_interactions))

    # find implicit interactions
    implicit_interactions = get_implicit_interactions(project_name, threads=1)
    implicit_paths = [i.interaction for i in implicit_interactions]
    print('Number implicit interactions:', len(implicit_interactions))
    print('Implicit Interactions:')
    for p in implicit_paths:
        pass#print(p)
    print()

    # component analysis
    component_implicit_frequency = generate_histogram(extract_components(implicit_paths))
    print("Component frequency in implicit interactions")
    for key, value in sorted(component_implicit_frequency.items(), key=lambda x: (x[1] == 'NaN', x[1]), reverse=True):
        print(key, '\t', value)
    print()
    component_intended_frequency = generate_histogram(extract_components(intended_interactions))
    print("Component frequency in intended interactions")
    for key, value in sorted(component_intended_frequency.items(), key=lambda x: (x[1] == 'NaN', x[1]), reverse=True):
        print(key, '\t', value)
    print()
    component_contribution_ratio = ratio(component_implicit_frequency, component_intended_frequency)
    print("Component contribution ratios")
    for key, value in sorted(component_contribution_ratio.items(), key=lambda x: (x[1] == 'NaN', x[1]), reverse=True):
        print(key, '\t', value)
    print()
    
    # direct interaction analysis
    DI_implicit_frequency = generate_histogram(list([o for o in implicit_paths for o in o]))
    print("Direct Interaction frequency in implicit interactions")
    for key, value in sorted(DI_implicit_frequency.items(), key=lambda x: (x[1] == 'NaN', x[1]), reverse=True):
        print(key, '\t', value)
    print()
    DI_intended_frequency = generate_histogram(list([o for o in intended_interactions for o in o]))
    print("Direct Interaction frequency in intended interactions")
    for key, value in sorted(DI_intended_frequency.items(), key=lambda x: (x[1] == 'NaN', x[1]), reverse=True):
        print(key, '\t', value)
    print()
    DI_contribution_ratio = ratio(DI_implicit_frequency, DI_intended_frequency)
    print("Direct interaction contribution ratios")
    for key, value in sorted(DI_contribution_ratio.items(), key=lambda x: (x[1] == 'NaN', x[1]), reverse=True):
        print(key, '\t', value)
    print()
                      
    # graph metrics
    print('Graph Metrics')
    print('Correlation between agent degrees (directed multigraph) and frequency in implicit interactions:', correlate_two_measures(mulitigraph_degree(communication_graph), component_implicit_frequency))
    print('Correlation between agent degrees (directed graph) and frequency in implicit interactions:', correlate_two_measures(degree(communication_graph), component_implicit_frequency))
    print('Correlation between agent page rank (directed graph) and frequency in implicit interactions:', correlate_two_measures(page_rank(communication_graph), component_implicit_frequency))
    print('Correlation between agent eigenvector (undirected graph) centralities and frequency in implicit interactions:', correlate_two_measures(eigenvector(communication_graph), component_implicit_frequency))
    print('Minimum edge cut size (undirected graph):', len(min_edge_cut(communication_graph)))
    print('Minimum node cut size (undirected graph):', len(min_node_cut(communication_graph)))
    print('Number bridges (undirected graph):', len(find_bridges(communication_graph)))
    print('Number cycles (directed graph):', len(cycles(communication_graph)))
    #print('Omega (undirected graph):', small_worldedness(communication_graph))
    print('Centralization (directed graph):', centralization(communication_graph))
    print('Degree variance (directed multigraph):', degree_variance(communication_graph))
    print()
        
    # print in LaTeX format
    print()
    print()
    print('Implicit Interactions:')
    #print_implicit_interactions(implicit_paths)
    print()
    print()
    print('LaTeX for direct interaction contribution ratios')
    for key, value in sorted(DI_contribution_ratio.items(), key=lambda x: (x[1] == 'NaN', x[1]), reverse=True):
      print('\t$\Agent{'+key[0]+'} '+'\\to'+key[1]+' \Agent{'+key[2]+'}$'+' & '+(str(round(value,4)) if not isinstance(value, str) else '-')+' \\\\')
      pass
    print()
    print()
    print('LaTeX for agent contribution ratios')
    for key, value in sorted(component_contribution_ratio.items(), key=lambda x: (x[1] == 'NaN', x[1]), reverse=True):
      print('\t$\Agent{'+key+'}$  & '+(str(round(value,4)) if not isinstance(value, str) else '-')+' \\\\')
      pass
    print()
    print()
    print_interaction_contribution_ratio_table(DI_implicit_frequency, DI_intended_frequency, {k: round(v,4) for k,v in DI_contribution_ratio.items()})
    
    #tree = get_intended_interaction_tree(project_name)
    #print(average_parallelism(tree, intended_interactions))
    configuration = json.loads(read_file(project_name + "/Data/Specs/" + project_name.split('/')[-1] + ".json"))
    first_occurence_differences = get_first_occurence_differences(configuration['edges'])
    print()
    print()
    print('LaTeX for difference between first occurence of agents')
    for key, value in sorted(first_occurence_differences.items(), key=lambda x: (x[1] == 'NaN', x[1]), reverse=True):
      print('\t$\Agent{'+key[0]+'} '+'\\to'+key[1]+' \Agent{'+key[2]+'}$'+' & '+(str(round(value,4)) if not isinstance(value, str) else '-')+' \\\\')
      pass
    
    print(first_occurence_differences.keys() == DI_contribution_ratio.keys())
    print(set(first_occurence_differences.keys()).difference(DI_contribution_ratio.keys()))
    print(set(DI_contribution_ratio.keys()).difference(first_occurence_differences.keys()))
    print('correlation with frequency in implicit interactions:', correlate_two_measures(DI_implicit_frequency, first_occurence_differences))
    print('correlation with contribution ratio:', correlate_two_measures(DI_contribution_ratio, first_occurence_differences))
    if implicit_interactions[0].exploitability is not None:
    	plot_histogram([x.exploitability for x in implicit_interactions],
                       title='Exploitability Frequencies', x_axis='Exploitability', y_axis='Frequency',
                       x_lim=(-0.1, 1.1),
                       save=False)
                       
    print({k:v*3 for k,v in mulitigraph_degree(communication_graph).items()})
    print(eigenvector(communication_graph))
    print(page_rank(communication_graph))
