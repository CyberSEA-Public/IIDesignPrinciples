import sys
import matplotlib.pyplot as plt


from src.input.read_outputs import get_implicit_interactions
from src.utilities.plotting import plot_histogram


SPECIFICATION_FOLDER = 'resources/data/'


implcit_interactions1 = get_implicit_interactions(SPECIFICATION_FOLDER + sys.argv[1], threads=1)
implcit_interactions2 = get_implicit_interactions(SPECIFICATION_FOLDER + sys.argv[2], threads=1)
paths1 = [x.interaction for x in implcit_interactions1]
paths2 = [x.interaction for x in implcit_interactions2]

print('paths not in', sys.argv[2], 'results')
distinct1 = 0
for p in paths1:
  if p not in paths2:
    print(p)
    distinct1 += 1
print()

print('paths not in', sys.argv[1], 'results')
distinct2 = 0
for q in paths2:
  if q not in paths1:
    print(q)
    distinct2 += 1
print()

print('number implicit interactions in', sys.argv[1], ':', len(implcit_interactions1))
print('number implicit interactions in', sys.argv[2], ':', len(implcit_interactions2))
print()
print('number implicit interactions in', sys.argv[1], 'not in', sys.argv[2], ':', distinct1)
print('number implicit interactions in', sys.argv[2], 'not in', sys.argv[1], ':', distinct2)

fig, ax = plt.subplots(2, 2, figsize=(12,12))
plot_histogram([x.exploitability for x in implcit_interactions1],
                       title='Exploitability Frequencies in '+sys.argv[1], x_axis='Exploitability', y_axis='Frequency',
                       x_lim=(-0.1, 1.1),
                       save=False, display=False, fig=ax[0,0])
plot_histogram([x.severity for x in implcit_interactions1],
                       title='Severity Frequencies in '+sys.argv[1], x_axis='Severity', y_axis='Frequency',
                       x_lim=(-0.1, 1.1),
                       save=False, display=False, fig=ax[1,0])
plot_histogram([x.exploitability for x in implcit_interactions2],
                       title='Exploitability Frequencies in '+sys.argv[2], x_axis='Exploitability', y_axis='Frequency',
                       x_lim=(-0.1, 1.1),
                       save=False, display=False, fig=ax[0,1])
plot_histogram([x.severity for x in implcit_interactions2],
                       title='Severity Frequencies in '+sys.argv[2], x_axis='Exploitability', y_axis='Frequency',
                       x_lim=(-0.1, 1.1),
                       save=False, display=True, fig=ax[1,1])
