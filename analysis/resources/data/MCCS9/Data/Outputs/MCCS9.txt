SoCA            : MCCS9
External Stimuli: {doneH, doneP, end, load, loaded, prepare, prepared, process, processed, ready, reset, setup, start, unload, unloaded, 𝖉, 𝖓}
Behaviours      : {EMPTY, FULL, IDLE, INIT, LOAD, MOVE, PREP, PROC, RESET, RESETH, RESETP, SET, STBY, UNLOAD, WAIT, WORK, 0, 1}
Named Constants : {GETMATERIAL, NULL, PROCESS}
Agents          : {C, H, P, S}


------------------------
INTENDED INTERACTIONS   
------------------------
	C  ->S  S  ->S  C  ->B  H  ->S  C  ->S  S  ->S  C  ->B  P  ->S  C  ->B  P  ->S  C  ->S  H  ->S  C
	C  ->S  S  ->S  C  ->B  H  ->S  C  ->S  S  ->S  C  ->B  P  ->S  C  ->B  P  ->S  C  ->S  P  ->S  C



C ~>+ H: True
------------------------
ALL PATHS: C ~>+ H
------------------------
	SEVERITY = 0.00		C  ->E  H
	SEVERITY = 0.00		C  ->S  H
------------------------
IMPLICIT PATHS: C ~>+ H
------------------------

C ~>+ P: True
------------------------
ALL PATHS: C ~>+ P
------------------------
	SEVERITY = 0.00		C  ->E  P
	SEVERITY = 0.00		C  ->S  P
------------------------
IMPLICIT PATHS: C ~>+ P
------------------------

C ~>+ S: True
------------------------
ALL PATHS: C ~>+ S
------------------------
	SEVERITY = 0.00		C  ->S  S
------------------------
IMPLICIT PATHS: C ~>+ S
------------------------

H ~>+ C: True
------------------------
ALL PATHS: H ~>+ C
------------------------
	SEVERITY = 0.00		H  ->S  C
------------------------
IMPLICIT PATHS: H ~>+ C
------------------------

H ~>+ P: True
------------------------
ALL PATHS: H ~>+ P
------------------------
	SEVERITY = 0.50		H  ->S  C  ->E  P
	SEVERITY = 0.50		H  ->S  C  ->S  P
------------------------
IMPLICIT PATHS: H ~>+ P
------------------------
	H  ->S  C  ->E  P
	H  ->S  C  ->S  P

H ~>+ S: True
------------------------
ALL PATHS: H ~>+ S
------------------------
	SEVERITY = 0.00		H  ->S  C  ->S  S
------------------------
IMPLICIT PATHS: H ~>+ S
------------------------

P ~>+ C: True
------------------------
ALL PATHS: P ~>+ C
------------------------
	SEVERITY = 0.00		P  ->S  C
------------------------
IMPLICIT PATHS: P ~>+ C
------------------------

P ~>+ H: True
------------------------
ALL PATHS: P ~>+ H
------------------------
	SEVERITY = 0.50		P  ->S  C  ->E  H
	SEVERITY = 0.00		P  ->S  C  ->S  H
------------------------
IMPLICIT PATHS: P ~>+ H
------------------------
	P  ->S  C  ->E  H

P ~>+ S: True
------------------------
ALL PATHS: P ~>+ S
------------------------
	SEVERITY = 0.50		P  ->S  C  ->S  S
------------------------
IMPLICIT PATHS: P ~>+ S
------------------------
	P  ->S  C  ->S  S

S ~>+ C: True
------------------------
ALL PATHS: S ~>+ C
------------------------
	SEVERITY = 0.00		S  ->S  C
------------------------
IMPLICIT PATHS: S ~>+ C
------------------------

S ~>+ H: True
------------------------
ALL PATHS: S ~>+ H
------------------------
	SEVERITY = 0.00		S  ->S  C  ->E  H
	SEVERITY = 0.00		S  ->S  C  ->S  H
------------------------
IMPLICIT PATHS: S ~>+ H
------------------------

S ~>+ P: True
------------------------
ALL PATHS: S ~>+ P
------------------------
	SEVERITY = 0.00		S  ->S  C  ->E  P
	SEVERITY = 0.00		S  ->S  C  ->S  P
------------------------
IMPLICIT PATHS: S ~>+ P
------------------------


------------------------
ANALYSIS SUMMARY        
------------------------
C ~>+ H: 0/2
C ~>+ P: 0/2
C ~>+ S: 0/1
H ~>+ C: 0/1
H ~>+ P: 2/2
H ~>+ S: 0/1
P ~>+ C: 0/1
P ~>+ H: 1/2
P ~>+ S: 1/1
S ~>+ C: 0/1
S ~>+ H: 0/2
S ~>+ P: 0/2
------------------------
TOTAL: 4/18
------------------------

