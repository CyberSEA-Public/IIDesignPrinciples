import itertools, json
from networkx.algorithms.centrality.degree_alg import out_degree_centrality
from networkx.algorithms.efficiency_measures import global_efficiency
from networkx.algorithms.shortest_paths.generic import average_shortest_path_length
import pandas as pd
import numpy as np

from statistics import mean, variance, median
from scipy.stats import skew
from src.input.read_outputs import get_implicit_interactions, get_intended_interactions, get_num_interactions
from src.input.read_specs import read_specs, get_intended_interaction_tree
from src.metrics.frequency_analysis import generate_histogram, ratio
from src.metrics.graphical_analysis import centralization, communication_graph_from_behaviors, cycles, degree_variance, find_bridges, min_edge_cut, min_node_cut, amount_broadcasting, small_worldedness, mulitigraph_degree, average_parallelism, fan_in, fan_out, eigenvector, page_rank
from src.output.output import print_full_dataframe, write_to_spreadsheet
from src.output.latex_formatting import print_dataframe_latex
from src.utilities.utilities import extract_components, combine_into_dataframe, read_file


SPECIFICATION_FOLDER = 'resources/data'

SYSTEM_TO_USE = ['MCCS1','MCCS2','MCCS3','MCCS4','MCCS5','MCCS6','MCCS7','MCCS8','MCCS9','MCCS10']


def correlate_two_measures(m1, m2):
    metrics = {'m1': m1, 'm2': m2}
    _, correlations = combine_into_dataframe(metrics, list(set.union(set(m1.keys()), set(m2.keys()))))
    return correlations['m1']['m2']

def analyze_system(system_name):
    print(system_name)
    project_name = SPECIFICATION_FOLDER + '/' + system_name
    
    configuration = json.loads(read_file(project_name + "/Data/Specs/" + project_name.split('/')[-1] + ".json"))

    state_transitions, concrete_behaviors, _ = read_specs(project_name)
    communication_graph = communication_graph_from_behaviors(state_transitions, concrete_behaviors, only_influencing_stimuli=False)
    intended_interactions = get_intended_interactions(project_name)
    intended_interaction_tree = get_intended_interaction_tree(project_name)
    implicit_interactions = get_implicit_interactions(project_name, threads=1)
    implicit_paths = [i.interaction for i in implicit_interactions]

    _, nun_total_interactions = get_num_interactions(project_name)
    num_behaviors = sum(len(x.items()) for x in concrete_behaviors.values())
    num_stimuli = len(set([x for x,_ in list(state_transitions.values())[0].keys()]))
    
    component_implicit_frequency = generate_histogram(extract_components(implicit_paths))
    component_intended_frequency = generate_histogram(extract_components(intended_interactions))
    component_contribution_ratio = ratio(component_implicit_frequency, component_intended_frequency)
    
    DI_implicit_frequency = generate_histogram(list([o for o in implicit_paths for o in o]))
    DI_intended_frequency = generate_histogram(list([o for o in intended_interactions for o in o]))
    DI_contribution_ratio = ratio(DI_implicit_frequency, DI_intended_frequency)

    cycles_list = cycles(communication_graph)

    return {
        'Number of Agents': len(component_contribution_ratio),
        'Number of Direct Interactions': len(DI_contribution_ratio),
        'Cyclomatic Complexity': len(DI_contribution_ratio) - len(component_contribution_ratio) + 2,
        'Number of Stimuli': num_stimuli,
        'Number of Behaviors': num_behaviors,
        "Number of 'boxes' in C2KA spec": num_behaviors * num_stimuli,
        'Number cycles': len(cycles_list),
        'Largest cycle': len(sorted(cycles_list, key=lambda x: len(x))[-1]) if len(cycles_list) > 0 else 0,
        'Median cycle size': median([len(x) for x in cycles_list]) if len(cycles_list) > 0 else 0,
        'Number of intended interactions:' : len(intended_interactions),
        'Degree variance': degree_variance(communication_graph)
        }


def build_dataset(systems, save_as_spreadsheet=False, print_results=False, output_spreadsheet_name='results'):
	x = {'MCCS'+str(systems.index(s)+1): analyze_system(s) for s in systems}
	x = pd.DataFrame.from_dict(x, orient='index')
	x.fillna(x.mean(), inplace=True)

	print_dataframe_latex(x)


def format_centrality_measures(systems):
	data = {}

	for system_name in systems:
	    print(system_name)
	    project_name = SPECIFICATION_FOLDER + '/' + system_name

	    state_transitions, concrete_behaviors, _ = read_specs(project_name)
	    communication_graph = communication_graph_from_behaviors(state_transitions, concrete_behaviors, only_influencing_stimuli=False)

	    data[system_name] = {
		'degree_centrality': mulitigraph_degree(communication_graph),
		'degree_in': fan_in(communication_graph),
		'degree_out': fan_out(communication_graph),
		'eigen': eigenvector(communication_graph),
		'pagerank': page_rank(communication_graph)
	    }
	print(data[system_name]['eigen'].keys())
	# map(int, [x*(len(data[system_name]['degree_centrality'])-1) for x in data[system_name]['degree_centrality'].values()])
	
	
	for measure in data[system_name].keys():
	    print(measure)
	    for system_name in systems:
	        print('\\ref{'+system_name + '} &\t' + '&\t'.join(map(str, 
			 data[system_name][measure].values()
	        )) + '\\\\')
	  

if __name__ == '__main__':
	#build_dataset(SYSTEM_TO_USE, save_as_spreadsheet=True, print_results=True,output_spreadsheet_name='system-level-measurements')
	format_centrality_measures(SYSTEM_TO_USE)
