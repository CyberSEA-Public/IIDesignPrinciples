import sys,json

import pandas as pd

from src.input.read_outputs import get_intended_interactions, get_implicit_interactions
from src.input.read_specs import read_specs, get_intended_interaction_tree
from src.metrics.frequency_analysis import generate_histogram, ratio
from src.metrics.graphical_analysis import communication_graph_from_behaviors, degree, eigenvector, page_rank, min_edge_cut, min_node_cut, find_bridges, cycles, mulitigraph_degree, small_worldedness, centralization, degree_variance, average_parallelism
from src.utilities.utilities import extract_components, combine_into_dataframe, read_file
from src.utilities.plotting import plot_histogram
from src.output.output import print_full_dataframe
from src.output.latex_formatting import print_implicit_interactions, print_interaction_contribution_ratio_table


SPECIFICATION_FOLDER = 'resources/data'

# the specific system specification folder to parse
PROJECT_SUBFOLDER = ManufacturingCell


def get_first_occurence_differences(edges):
    first_occurence = {}
    direct_interactions = set()
    
    for edge in edges:
        if edge[2] == 'B':
            direct_interactions.add((edge[0][1], 'S', edge[1][1]))
            direct_interactions.add((edge[0][1], 'E', edge[1][1]))
        else:
    	    direct_interactions.add((edge[0][1], edge[2], edge[1][1]))
 
        if edge[0][1] not in first_occurence.keys():
    	    first_occurence[edge[0][1]] = edge[0][0]
        if edge[1][1] not in first_occurence.keys():
    	    first_occurence[edge[1][1]] = edge[1][0]

    return {DI: abs(first_occurence[DI[0]]-first_occurence[DI[-1]]) for DI in direct_interactions}


def correlate_two_measures(m1, m2):
    metrics = {'m1': m1, 'm2': m2}
    _, correlations = combine_into_dataframe(metrics, list(set.union(set(m1.keys()), set(m2.keys()))))
    return correlations['m1']['m2']


if __name__ == '__main__':
    project_name = SPECIFICATION_FOLDER + '/' + PROJECT_SUBFOLDER

    # read the agent specification files for the project into a dictionary
    #
    # each item maps an agent name to a dictionary that describes the agent's specifications
    #
    # each item in the specification maps a pair of incoming stimulus and start state to a
    # pair of next stimulus and next state
    state_transitions, concrete_behaviors, stimuli_from_concrete_behavior = read_specs(project_name)

    # build a communication graph of the system using the agents specifications
    #
    # This will show all the ways each agent can influence, and be influenced by, other agents in the system
    communication_graph = communication_graph_from_behaviors(state_transitions, concrete_behaviors, only_influencing_stimuli=False)

    # parse the intended interactions listed in the C2KA-Tool output file for a system into a list
    #
    # each element in the list represents an intended interaction, and is a list of tuples
    #
    # each tuple in an intended interaction is one direct interaction of the form:
    #       (<source>, <interaction type>, <sink>)
    intended_interactions = get_intended_interactions(project_name)

    # results
    print('System:', project_name)
    print()
    print('Communication Graph:')
    print(communication_graph.label_edges_by_interaction_type().as_adjacency_matrix_string())
    print(communication_graph.as_adjacency_matrix_string())
    print()
    print('Number intended interactions:', len(intended_interactions))

    
                      
    
