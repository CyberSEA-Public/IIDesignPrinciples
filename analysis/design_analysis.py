import json, sys

from networkx import number_of_nodes

from src.input.read_outputs import get_intended_interactions
from src.input.read_specs import read_specs, get_intended_interaction_tree
from src.metrics.frequency_analysis import generate_histogram
from src.metrics.graphical_analysis import communication_graph_from_behaviors, cycles, mulitigraph_degree, subgraph_coupling
from src.utilities.utilities import read_file, flatten_list


SPECIFICATION_FOLDER = 'resources/data'


def extract_DIs(path_list):
    DI_list = list()
    for path in path_list:
        for i in range(len(path)-1):
            DI_list.append((path[i],path[i+1]))
        DI_list.append((path[-1],path[0]))
    return DI_list


def analyze_system(system_name):
    project_name = SPECIFICATION_FOLDER + '/' + system_name
    
    configuration = json.loads(read_file(project_name + "/Data/Specs/" + project_name.split('/')[-1] + ".json"))

    state_transitions, concrete_behaviors, _ = read_specs(project_name)
    communication_graph = communication_graph_from_behaviors(state_transitions, concrete_behaviors, only_influencing_stimuli=False)
    intended_interactions = get_intended_interactions(project_name)
    intended_interaction_tree = get_intended_interaction_tree(project_name)

    num_behaviors = sum(len(x.items()) for x in concrete_behaviors.values())
    num_stimuli = len(set([x for x,_ in list(state_transitions.values())[0].keys()]))
    
    external_stimuli = configuration['external_stim']


    c = cycles(communication_graph)
  
    return {
        'Degree Centrality': {k:v*(communication_graph.to_networkx_multigraph().number_of_nodes()-1) for k,v in mulitigraph_degree(communication_graph).items()},
        'Cycle Agent Frequency': generate_histogram(flatten_list(c)),
        'Cycle Interaction Frequency': generate_histogram(extract_DIs(c)),
        'Largest Cycle': max(c, key=len),
        'Normalized Subgraph Coupling': {k:v for k,v in subgraph_coupling(communication_graph).items() if v > 0}
        }


def format_dict(x, title):
    print(title)
    for k,v in sorted(x[title].items(), key=lambda x: x[1] , reverse=True):
    	print('\t',k,': ',v)
    print()
    

if __name__ == '__main__':
	x = analyze_system(sys.argv[1]) 
	
	format_dict(x, 'Degree Centrality')
	format_dict(x, 'Normalized Subgraph Coupling')
	format_dict(x, 'Cycle Agent Frequency')
	format_dict(x, 'Cycle Interaction Frequency')
	
	print('Largest Cycle')
	print(x['Largest Cycle'])
	

	
