SoCA            : MCCS4
External Stimuli: {done, end, load, loaded, prepare, process, processed, ready, setup, start, unload, unloaded, 𝖉, 𝖓}
Behaviours      : {EMPTY, FULL, IDLE, INIT, LOAD, MOVE, PREP, PROC, SET, STBY, WAIT, WORK, 0, 1}
Named Constants : {GETMATERIAL, NULL, PROCESS}
Agents          : {C, H, P, S}


------------------------
INTENDED INTERACTIONS   
------------------------
	C  ->S  S  ->S  C  ->B  H  ->S  S  ->S  C  ->B  P  ->S  H  ->S  P  ->S  C
	C  ->S  S  ->S  C  ->B  H  ->S  S  ->S  C  ->B  P  ->S  H  ->S  C  ->S  P



C ~>+ H: True
------------------------
ALL PATHS: C ~>+ H
------------------------
	SEVERITY = 0.00		C  ->E  P  ->S  H
	SEVERITY = 0.00		C  ->S  P  ->S  H
	SEVERITY = 0.00		C  ->E  H
	SEVERITY = 0.00		C  ->S  H
------------------------
IMPLICIT PATHS: C ~>+ H
------------------------

C ~>+ P: True
------------------------
ALL PATHS: C ~>+ P
------------------------
	SEVERITY = 0.50		C  ->E  H  ->S  P
	SEVERITY = 0.50		C  ->S  H  ->S  P
	SEVERITY = 0.00		C  ->E  P
	SEVERITY = 0.00		C  ->S  P
------------------------
IMPLICIT PATHS: C ~>+ P
------------------------
	C  ->E  H  ->S  P
	C  ->S  H  ->S  P

C ~>+ S: True
------------------------
ALL PATHS: C ~>+ S
------------------------
	SEVERITY = 0.00		C  ->S  S
	SEVERITY = 0.33		C  ->E  P  ->S  H  ->S  S
	SEVERITY = 0.33		C  ->S  P  ->S  H  ->S  S
	SEVERITY = 0.00		C  ->E  H  ->S  S
	SEVERITY = 0.00		C  ->S  H  ->S  S
------------------------
IMPLICIT PATHS: C ~>+ S
------------------------
	C  ->E  P  ->S  H  ->S  S
	C  ->S  P  ->S  H  ->S  S

H ~>+ C: True
------------------------
ALL PATHS: H ~>+ C
------------------------
	SEVERITY = 0.00		H  ->S  C
	SEVERITY = 0.00		H  ->S  P  ->S  C
	SEVERITY = 0.00		H  ->S  S  ->S  C
------------------------
IMPLICIT PATHS: H ~>+ C
------------------------

H ~>+ P: True
------------------------
ALL PATHS: H ~>+ P
------------------------
	SEVERITY = 0.00		H  ->S  P
	SEVERITY = 0.50		H  ->S  C  ->E  P
	SEVERITY = 0.00		H  ->S  S  ->S  C  ->E  P
	SEVERITY = 0.00		H  ->S  C  ->S  P
	SEVERITY = 0.00		H  ->S  S  ->S  C  ->S  P
------------------------
IMPLICIT PATHS: H ~>+ P
------------------------
	H  ->S  C  ->E  P

H ~>+ S: True
------------------------
ALL PATHS: H ~>+ S
------------------------
	SEVERITY = 0.50		H  ->S  C  ->S  S
	SEVERITY = 0.33		H  ->S  P  ->S  C  ->S  S
	SEVERITY = 0.00		H  ->S  S
------------------------
IMPLICIT PATHS: H ~>+ S
------------------------
	H  ->S  C  ->S  S
	H  ->S  P  ->S  C  ->S  S

P ~>+ C: True
------------------------
ALL PATHS: P ~>+ C
------------------------
	SEVERITY = 0.00		P  ->S  H  ->S  C
	SEVERITY = 0.00		P  ->S  C
	SEVERITY = 0.33		P  ->S  H  ->S  S  ->S  C
------------------------
IMPLICIT PATHS: P ~>+ C
------------------------
	P  ->S  H  ->S  S  ->S  C

P ~>+ H: True
------------------------
ALL PATHS: P ~>+ H
------------------------
	SEVERITY = 0.00		P  ->S  H
	SEVERITY = 0.50		P  ->S  C  ->E  H
	SEVERITY = 0.50		P  ->S  C  ->S  H
------------------------
IMPLICIT PATHS: P ~>+ H
------------------------
	P  ->S  C  ->E  H
	P  ->S  C  ->S  H

P ~>+ S: True
------------------------
ALL PATHS: P ~>+ S
------------------------
	SEVERITY = 0.33		P  ->S  H  ->S  C  ->S  S
	SEVERITY = 0.50		P  ->S  C  ->S  S
	SEVERITY = 0.50		P  ->S  H  ->S  S
	SEVERITY = 0.33		P  ->S  C  ->E  H  ->S  S
	SEVERITY = 0.33		P  ->S  C  ->S  H  ->S  S
------------------------
IMPLICIT PATHS: P ~>+ S
------------------------
	P  ->S  H  ->S  C  ->S  S
	P  ->S  C  ->S  S
	P  ->S  H  ->S  S
	P  ->S  C  ->E  H  ->S  S
	P  ->S  C  ->S  H  ->S  S

S ~>+ C: True
------------------------
ALL PATHS: S ~>+ C
------------------------
	SEVERITY = 0.00		S  ->S  C
------------------------
IMPLICIT PATHS: S ~>+ C
------------------------

S ~>+ H: True
------------------------
ALL PATHS: S ~>+ H
------------------------
	SEVERITY = 0.00		S  ->S  C  ->E  P  ->S  H
	SEVERITY = 0.00		S  ->S  C  ->S  P  ->S  H
	SEVERITY = 0.00		S  ->S  C  ->E  H
	SEVERITY = 0.00		S  ->S  C  ->S  H
------------------------
IMPLICIT PATHS: S ~>+ H
------------------------

S ~>+ P: True
------------------------
ALL PATHS: S ~>+ P
------------------------
	SEVERITY = 0.33		S  ->S  C  ->E  H  ->S  P
	SEVERITY = 0.33		S  ->S  C  ->S  H  ->S  P
	SEVERITY = 0.00		S  ->S  C  ->E  P
	SEVERITY = 0.00		S  ->S  C  ->S  P
------------------------
IMPLICIT PATHS: S ~>+ P
------------------------
	S  ->S  C  ->E  H  ->S  P
	S  ->S  C  ->S  H  ->S  P


------------------------
ANALYSIS SUMMARY        
------------------------
C ~>+ H: 0/4
C ~>+ P: 2/4
C ~>+ S: 2/5
H ~>+ C: 0/3
H ~>+ P: 1/5
H ~>+ S: 2/3
P ~>+ C: 1/3
P ~>+ H: 2/3
P ~>+ S: 5/5
S ~>+ C: 0/1
S ~>+ H: 0/4
S ~>+ P: 2/4
------------------------
TOTAL: 17/44
------------------------

