SoCA            : MCCS8
External Stimuli: {end, loaded, process, ready, start, unload, unloaded, 𝖉, 𝖓}
Behaviours      : {EMPTY, FULL, MOVE, SET, WAIT, WORK, 0, 1}
Named Constants : {NULL, PROCESS}
Agents          : {H, P, S}


------------------------
INTENDED INTERACTIONS   
------------------------
	S  ->S  H  ->S  S  ->S  P  ->S  H  ->S  P
	S  ->S  H  ->E  P  ->S  H  ->S  P



H ~>+ P: True
------------------------
ALL PATHS: H ~>+ P
------------------------
	SEVERITY = 0.00		H  ->S  S  ->S  P
	SEVERITY = 0.00		H  ->E  P
	SEVERITY = 0.00		H  ->S  P
------------------------
IMPLICIT PATHS: H ~>+ P
------------------------

H ~>+ S: True
------------------------
ALL PATHS: H ~>+ S
------------------------
	SEVERITY = 0.00		H  ->S  S
------------------------
IMPLICIT PATHS: H ~>+ S
------------------------

P ~>+ H: True
------------------------
ALL PATHS: P ~>+ H
------------------------
	SEVERITY = 0.00		P  ->S  H
------------------------
IMPLICIT PATHS: P ~>+ H
------------------------

P ~>+ S: True
------------------------
ALL PATHS: P ~>+ S
------------------------
	SEVERITY = 0.50		P  ->S  H  ->S  S
------------------------
IMPLICIT PATHS: P ~>+ S
------------------------
	P  ->S  H  ->S  S

S ~>+ H: True
------------------------
ALL PATHS: S ~>+ H
------------------------
	SEVERITY = 0.00		S  ->S  P  ->S  H
	SEVERITY = 0.00		S  ->S  H
------------------------
IMPLICIT PATHS: S ~>+ H
------------------------

S ~>+ P: True
------------------------
ALL PATHS: S ~>+ P
------------------------
	SEVERITY = 0.00		S  ->S  P
	SEVERITY = 0.00		S  ->S  H  ->E  P
	SEVERITY = 0.50		S  ->S  H  ->S  P
------------------------
IMPLICIT PATHS: S ~>+ P
------------------------
	S  ->S  H  ->S  P


------------------------
ANALYSIS SUMMARY        
------------------------
H ~>+ P: 0/3
H ~>+ S: 0/1
P ~>+ H: 0/1
P ~>+ S: 1/1
S ~>+ H: 0/2
S ~>+ P: 1/3
------------------------
TOTAL: 2/11
------------------------

