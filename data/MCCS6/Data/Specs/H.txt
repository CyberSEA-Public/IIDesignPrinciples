begin AGENT where
	H := WAIT + MOVE
end
begin NEXT_BEHAVIOUR where

	(start,WAIT)  = WAIT
	(start,MOVE)  = MOVE
	
	(load,WAIT)  = WAIT
	(load,MOVE)  = MOVE
	
	(loaded,WAIT)  = WAIT
	(loaded,MOVE)  = MOVE
	
	(prepare,WAIT)  = MOVE
	(prepare,MOVE)  = MOVE
	
	(prepared,WAIT)  = WAIT
	(prepared,MOVE)  = MOVE
	
	(unload,WAIT)  = WAIT
	(unload,MOVE)  = MOVE
	
	(unloaded,WAIT)  = WAIT
	(unloaded,MOVE)  = MOVE
	
	(setup,WAIT)  = WAIT
	(setup,MOVE)  = MOVE
	
	(ready,WAIT)  = WAIT
	(ready,MOVE)  = MOVE
	
	(process,WAIT)  = WAIT
	(process,MOVE)  = MOVE
	
	(processed,WAIT)  = WAIT
	(processed,MOVE)  = MOVE
	
	(resetH,WAIT)  = WAIT
	(resetH,MOVE)  = WAIT
	
	(doneH,WAIT) = WAIT
	(doneH,MOVE) = MOVE
	
	(resetP,WAIT) = WAIT
	(resetP,MOVE) = MOVE
	
	(doneP,WAIT) = WAIT
	(doneP,MOVE) = MOVE
	
	(end,WAIT)  = WAIT
	(end,MOVE)  = MOVE

end
begin NEXT_STIMULUS where

	(start,WAIT)  = N
	(start,MOVE)  = N
	
	(load,WAIT)  = N
	(load,MOVE)  = N
	
	(loaded,WAIT)  = N
	(loaded,MOVE)  = N
	
	(prepare,WAIT)  = prepared 
	(prepare,MOVE)  = N
	
	(prepared,WAIT)  = N 
	(prepared,MOVE)  = N
	
	(unload,WAIT)  = N
	(unload,MOVE)  = N
	
	(unloaded,WAIT)  = N
	(unloaded,MOVE)  = N
	
	(setup,WAIT)  = N
	(setup,MOVE)  = N
	
	(ready,WAIT)  = N
	(ready,MOVE)  = N
	
	(process,WAIT)  = N
	(process,MOVE)  = N
	
	(processed,WAIT)  = N
	(processed,MOVE)  = N
	
	(resetH,WAIT)  = N
	(resetH,MOVE) = doneH
	
	(doneH,WAIT) = N
	(doneH,MOVE) = N
	
	(resetP,WAIT) = N
	(resetP,MOVE) = N
	
	(doneP,WAIT) = N
	(doneP,MOVE) = N
	
	(end,WAIT)  = N
	(end,MOVE)  = N

end
begin CONCRETE_BEHAVIOUR where
	WAIT => [ material := NULL ]
	MOVE => [ material := GETMATERIAL ]	
end
