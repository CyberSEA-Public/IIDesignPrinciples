begin AGENT where

	H := WAIT + MOVE

end


begin NEXT_BEHAVIOUR where

	(start,WAIT)  = WAIT
	(start,MOVE)  = MOVE
	
	(loaded,WAIT)  = MOVE
	(loaded,MOVE)  = MOVE
	
	(unload,WAIT)  = WAIT
	(unload,MOVE)  = MOVE
	
	(unloaded,WAIT)  = WAIT
	(unloaded,MOVE)  = MOVE

	(ready,WAIT)  = WAIT
	(ready,MOVE)  = WAIT
	
	(process,WAIT)  = WAIT
	(process,MOVE)  = MOVE
	
	(end,WAIT)  = WAIT
	(end,MOVE)  = MOVE

end


begin NEXT_STIMULUS where

	(start,WAIT)  = N
	(start,MOVE)  = N
	
	(load,WAIT)  = N
	(load,MOVE)  = N
	
	(loaded,WAIT)  = unload
	(loaded,MOVE)  = N
	
	(unload,WAIT)  = N
	(unload,MOVE)  = N
	
	(unloaded,WAIT)  = N
	(unloaded,MOVE)  = N
	
	(ready,WAIT)  = N
	(ready,MOVE)  = process
	
	(process,WAIT)  = N
	(process,MOVE)  = N
	
	(end,WAIT)  = N
	(end,MOVE)  = N
	
end


begin CONCRETE_BEHAVIOUR where

	WAIT => [ material := 0 ]
	MOVE => [ if  (status = 1) -> material := 1 
			   | ~(status = 1) -> material := 0
			  fi ]	
	
end
