# Integrating Formal and Data Analyses to Identify Design Principles for Mitigating Implicit Interactions

## Description
This repository contains files used to model and analyze multiple design alternatives of a manufacturing cell control system (MCCS) described in an eponymous research paper.

## Technologies
This project uses the following tools:
- [C2KA-Tool](https://carleton.ca/cybersea/implicit-interactions-analysis-tool/)
- [Python 3.8+](https://www.python.org/) (The version is a requirement of the packages used)

### Python packages required
The latest stable versions of each external Python package are required
- [NumPy](https://numpy.org/)
- [Pandas](https://pandas.pydata.org/)
- [scikit-learn](https://scikit-learn.org/stable/)
- [NetworkX](https://networkx.org/)
- [openpyxl](https://pypi.org/project/openpyxl/)

These can be installed through the command line with the package manager that comes with your Python distribution (e.g., pip, conda) like below:
> pip3 install numpy pandas scikit-learn networkx openpyxl

## Folders
- **analysis** contains all of the Python scripts and data resources to run all of the data analysis activities of the approach described in the paper.
- **data** contains the C<sup>2</sup>KA specification for use with the C2KA-Tool, as well as the output of the formal implicit interactions analysis resulting from the C2KA-Tool.
- **results** contains the implicit interactions data set and the graph measurements dataset as well as the resulting correlations from the data.

## Usage
To reproduce the paper results, run *main.py* in your preferred IDE or via the command line:
> python3 main.py

The results will be saved into *analysis/resources/results*

